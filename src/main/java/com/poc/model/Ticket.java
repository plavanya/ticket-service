package com.poc.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long ticket_id;
	private String name;
	private int age;

	private String sex;

	private String from_address;
	
	private String to_address;
	
	public String getSex() {
		return sex;
	}

	public Ticket(Long ticket_id, String name, int age, String sex, String from_address, String to_address,
			Integer cost_ticket) {
		super();
		this.ticket_id = ticket_id;
		this.name = name;
		this.age = age;
		this.sex = sex;
		this.from_address = from_address;
		this.to_address = to_address;
		this.cost_ticket = cost_ticket;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	private Integer cost_ticket;


	public String getFrom_address() {
		return from_address;
	}

	public void setFrom_address(String from_address) {
		this.from_address = from_address;
	}

	public String getTo_address() {
		return to_address;
	}

	public void setTo_address(String to_address) {
		this.to_address = to_address;
	}

	public Integer getCost_ticket() {
		return cost_ticket;
	}

	public void setCost_ticket(Integer cost_ticket) {
		this.cost_ticket = cost_ticket;
	}

	public Ticket() {

	}

	public Long getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(Long ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
