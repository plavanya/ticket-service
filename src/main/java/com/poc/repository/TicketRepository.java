package com.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.poc.model.Ticket;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

}
