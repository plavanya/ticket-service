package com.poc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.model.Ticket;
import com.poc.repository.TicketRepository;

@Service
public class TicketService {
	@Autowired
	TicketRepository repository;

	public Ticket bookTicket(Ticket tkct) {
		return repository.save(tkct);
	}

	public Ticket updateTicketById(Long id, Ticket tkct) {

		Ticket ticket = repository.findById(id).orElse(null);
		ticket.setName(tkct.getName());
		ticket.setAge(tkct.getAge());
		ticket.setCost_ticket(tkct.getCost_ticket());
		ticket.setFrom_address(tkct.getFrom_address());
		ticket.setTo_address(tkct.getTo_address());
		ticket.setSex(tkct.getSex());
        System.out.println("update tick");
		return repository.save(ticket);
	}

	public void deleteTicketByid(Long id) {
		// TODO Auto-generated method stub
		repository.deleteById(id);
	}

	public Ticket getTicketById(Long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).orElse(null);
	}

	public List<Ticket> findAllTickets() {

		return repository.findAll();
	}
}
