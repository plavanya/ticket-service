package com.poc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.poc.model.Ticket;
import com.poc.service.TicketService;

@RestController
public class TicketController {
	@Autowired
	TicketService service;

	@PostMapping("/book")
	public Ticket bookTiket(@RequestBody Ticket tct) {
		return service.bookTicket(tct);
	}

	@PutMapping("/book/{id}")
	public Ticket updateById(@PathVariable(value = "id") Long tctId, @Valid @RequestBody Ticket tcket) {

		return service.updateTicketById(tctId, tcket);

	}

	@DeleteMapping("/deleteTicketByid/{id}")
	public void deleteTicketByid(@PathVariable Long id) {

		service.deleteTicketByid(id);
	}

	@GetMapping("/getTicketByid/{id}")
	public Ticket getTicketById(@PathVariable Long id) {
		return service.getTicketById(id);
	}

	@GetMapping("/findAll")
	public List<Ticket> getAllcsts() {

		return service.findAllTickets();
	}
	
	@GetMapping("/find")
	public List<Ticket> find() {

		return service.findAllTickets();
	}

	@GetMapping("/getTicket")
	public Ticket getTicket() {

		return new Ticket(12l, "ankamma", 28, "male", "hyderabad", "lbnagar", 20);
	}

}
